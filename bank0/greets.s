.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"

.segment "ZEROPAGE"
c_initdone = localramstart
scroll_counter= c_initdone+0
soft_scroll= c_initdone+1
textptr:= c_initdone+2
ypos= c_initdone+4
number_of_lines = c_initdone+5
offset = c_initdone+6
paintloop = c_initdone+7

NUMBER_OF_LINES = 13

RODATA_SEGMENT

; This table converts the "remainder" of the division by 15 (-1 to -15) to the correct
; fine adjustment value. This table is on a page boundary to guarantee the processor
; will cross a page boundary and waste a cycle in order to be at the precise position
; for a RESP0,x write
            .align $100

fineAdjustBegin:
            .byte %01110000; Left 7 
            .byte %01100000; Left 6
            .byte %01010000; Left 5
            .byte %01000000; Left 4
            .byte %00110000; Left 3
            .byte %00100000; Left 2
            .byte %00010000; Left 1
            .byte %00000000; No movement.
            .byte %11110000; Right 1
            .byte %11100000; Right 2
            .byte %11010000; Right 3
            .byte %11000000; Right 4
            .byte %10110000; Right 5
            .byte %10100000; Right 6
            .byte %10010000; Right 7

fineAdjustTable = fineAdjustBegin - %11110001; NOTE: %11110001 = -15


; Positions an object horizontally
; Inputs: A = Desired position.
; X = Desired object to be positioned (0-4).
;0 = Player0
;1 = Player1
;2 = Missile0
;3 = Missile1
;4 = Ball
; scanlines: If control comes on or before cycle 73 then 1 scanline is consumed.
; If control comes after cycle 73 then 2 scanlines are consumed.
; Outputs: X = unchanged

; A = Fine Adjustment value.
; Y = the "remainder" of the division by 15 minus an additional 15.
; control is returned on cycle 6 of the next scanline.

PosObject:
            sta WSYNC                  ; 00     Sync to start of scanline.
            sec                        ; 02     Set the carry flag so no borrow will be applied during the division.
divideby15: sbc #15                    ; 04     Waste the necessary amount of time dividing X-pos by 15!
            bcs divideby15            ; 06/07  11/16/21/26/31/36/41/46/51/56/61/66
            tay
            lda fineAdjustTable,y      ; 13 -> Consume 5 cycles by guaranteeing we cross a page boundary
            sta HMP0,x
            sta RESP0,x                ; 21/ 26/31/36/41/46/51/56/61/66/71 - Set the rough position.
            rts

pfcolortab:   .byte $c4,$a4,$86,$66,$66,$86,$a4,$c4


greetings:
	lda   textptr+1
        bne   greetings_noinit
        ldx   #paintloop_len-1
do_copy_paintloop:
        lda   paintloop_copy,x
        sta   paintloop,x
        dex
        bpl   do_copy_paintloop
        inx
        stx   PF0
        stx   PF1
        stx   PF2
        lda   #$0c
        sta   soft_scroll
        lda   #<greets
        sta   textptr
        lda   #>greets
        sta   textptr+1
greetings_noinit:
        lda   #$ff
        sta   COLUP0
        sta   COLUP1
        lda   #$13
        jsr   PosObject
        ldx   #$01
        lda   #$73
        jsr   PosObject
        ;ldx   #$04
        ;lda   #$64
        ;jsr   PosObject
        sta   WSYNC
        sta   HMOVE
        lda   #$06
        sta   NUSIZ0
        lda   #$02
        sta   NUSIZ1
        ;sta   ENABL
        lda   #$44
        sta   COLUPF
        ;lda   #%01110000
        ;sta   PF0
        ;lda   #%11001110
        ;sta   PF2
        ;lda   #%00010000
        ;sta   CTRLPF
        lda   #%00000001
        sta   CTRLPF
        lax   soft_scroll
        axs   #$2
        stx   soft_scroll
        stx   number_of_lines
        bcs   no_scroll_up
        txa
        axs   #$100-$0c
        stx   soft_scroll
        stx   number_of_lines
        lda   textptr
        clc
        adc   #$05
        sta   textptr
        bcc   no_scroll_up
        inc   textptr+1
no_scroll_up:
        jsr   waitvblank
        lda   #NUMBER_OF_LINES
        sta   scroll_counter
        lda   #$00
        sta   ypos
scroll_again:
        lda   number_of_lines
        cmp   #$07
        bcc   scroll_continue
        ldy   ypos
        lda   GFX,y
        inc   ypos
        sta   WSYNC
        sta   PF1
        dec   number_of_lines
        bne   scroll_again
scroll_continue:
        tax
        lda   pfcolortab,x
        sta   COLUPF
greets_loop:
        sta   WSYNC
        ldy   ypos
        inc   ypos
        lda   GFX,y
        sta   PF1
        ldy   #$00
        lax   (textptr),y
        lda   charsetptr_low,x
        sta   charoff0+paintloop
        lda   charsetptr_high,x
        sta   charoff0+paintloop+1
        iny
        lax   (textptr),y
        lda   charsetptr_low,x
        sta   charoff1+paintloop
        lda   charsetptr_high,x
        sta   charoff1+paintloop+1
        ldx   ypos
        inc   ypos
        lda   GFX,x
        sta   PF1
        iny
        lax   (textptr),y
        lda   charsetptr_low,x
        sta   charoff2+paintloop
        lda   charsetptr_high,x
        sta   charoff2+paintloop+1
        iny
        lax   (textptr),y
        lda   charsetptr_low,x
        sta   charoff3+paintloop
        lda   charsetptr_high,x
        sta   charoff3+paintloop+1
        iny
        lax   (textptr),y
        ldy   ypos
        inc   ypos
        lda   charsetptr_low,x
        sta   charoff4+paintloop
        lda   GFX,y
        sta   PF1
        lda   charsetptr_high,x
        sta   charoff4+paintloop+1
        lda   #$05
        clc
        adc   textptr
        sta   textptr
        bcc   no_text_high
        inc   textptr+1
no_text_high:        
        ldx   number_of_lines
        jsr   paintloop
paintloop_back:
        inx
        stx   GRP0
        stx   GRP1
        ldy   ypos
        lda   GFX,y
        sta   PF1
        inc   ypos
        
        lda   #$07
        sta   number_of_lines
        dec   scroll_counter
        beq   no_greets_loop
        jmp   greets_loop
no_greets_loop:
        ldx   soft_scroll
        lda   offset_of_lines_tab,x
        sta   offset
        lda   number_of_lines_tab,x
        bmi   no_more_to_paint
        sta   number_of_lines
        ldy   #$00
        lax   (textptr),y
        lda   charsetptr_low,x
        ora   offset
        sta   charoff0+paintloop
        lda   charsetptr_high,x
        sta   charoff0+paintloop+1
        iny
        lax   (textptr),y
        lda   charsetptr_low,x
        ora   offset
        sta   charoff1+paintloop
        lda   charsetptr_high,x
        sta   charoff1+paintloop+1
        iny
        lax   (textptr),y
        lda   charsetptr_low,x
        ora   offset
        sta   charoff2+paintloop
        lda   charsetptr_high,x
        sta   charoff2+paintloop+1
        iny
        lax   (textptr),y
        lda   charsetptr_low,x
        ora   offset
        sta   charoff3+paintloop
        lda   charsetptr_high,x
        sta   charoff3+paintloop+1
        iny
        lax   (textptr),y
        lda   charsetptr_low,x
        ora   offset
        sta   charoff4+paintloop
        lda   charsetptr_high,x
        sta   charoff4+paintloop+1
        ldx   number_of_lines
        sta   WSYNC
        jsr   paintloop
        inx
        stx   GRP0
        stx   GRP1
no_more_to_paint:
        ldy   #$00
        lda   (textptr),y
        bne   no_endofpart
        cli
no_endofpart:
        lda   textptr
        sec
        sbc   #NUMBER_OF_LINES*5
        sta   textptr
        lda   textptr+1
        sbc   #$00
        sta   textptr+1

        bankjmp global_scroller

        .align 16
number_of_lines_tab:
        .byte 6,5,4,3,2,1,0,$ff,$ff,$ff,$ff,$ff
offset_of_lines_tab:
        .byte 1,2,3,4,5,6,7,0,0,0,0


paintloop_copy:
paint_inner_loop:
        sta   WSYNC
        ldy   ypos
        lda   GFX,y
        sta   PF1
charoff0 = *-paintloop_copy+1
        lda   $ffff,x
        sta   GRP0
        inc   ypos
charoff1 = *-paintloop_copy+1
        lda   $ffff,x
        sta   GRP0
pfcolortaboff = *-paintloop_copy+1
        ldy   pfcolortab,x
charoff2 = *-paintloop_copy+1
        lda   $ffff,x
        sta   GRP0
        sty   COLUPF
charoff3 = *-paintloop_copy+1
        lda   $ffff,x
        ldy   #$00
        sty   $2f       ;nop
        sta   GRP1
charoff4 = *-paintloop_copy+1
        lda   $ffff,x
        sta   GRP1
        dex
        bpl   paint_inner_loop
        rts

paintloop_len = * - paintloop_copy

greets:
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .include "greetlist.inc"
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte "     "
        .byte 0,0,0,0,0
