
.include "globals.inc"

.segment "ZEROPAGE"

; must be first, at $80
schedule:
   .byte 0

temp8:
   .byte 0
temp16:
   .word 0

psmkAttenuation:
   .byte 0
psmkBeatIdx:
   .byte 0
psmkPatternIdx:
   .byte 0
psmkTempoCount:
   .byte 0

pffb: ; persistent: framebuffer
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00

delay:
   .byte $00   ; persistent: delay for next call to move
txtoff:
   .byte $00   ; persistent: index for next char in text buffer
currentbit:
   .byte $00

localramstart:
