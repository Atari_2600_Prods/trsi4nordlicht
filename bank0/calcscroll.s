
.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"

CODE_SEGMENT

wait5:
   lda   txtoff
   cmp   #$06
   bcc   :+
   cli
:
   jsr   waitvblank
   bankjmp global_scroller

lastframe:
   lda   #$01
   sta   schedule
   jsr   waitvblank
   cli
   bankjmp global_scroller

firstframe:
   lda   #$80
   sta   currentbit
   bit   INPT4
   bpl   :+
   cli
:
   jsr   waitvblank
cont_scroll:
   jsr   calcscroll
   jsr   waitscreen
   jmp   waitoverscan

calcscroll:
   dec delay
   bpl noscroll
   lda #SCROLLDELAY
   sta delay
   ldx txtoff
   lda text,x
   beq :+
   and #$7f ; unneccessary
   sec
   sbc #$20
:
   ldx #$00
   stx temp16+1
   asl
   rol temp16+1
   rol
   rol temp16+1
   rol
   rol temp16+1
   sta temp16
   clc
   lda #<scrollcharset
   adc temp16
   sta temp16
   lda #>scrollcharset
   adc temp16+1
   sta temp16+1

   ldx #$07
scroll:
   txa
   tay

   lda (temp16),y
   and currentbit
   cmp currentbit

   ; carry contains "new bit to draw"
   ror pffb+$28,x
   rol pffb+$20,x
   ror pffb+$18,x
   lda pffb+$18,x
   and #$08
   cmp #$08
   ror pffb+$10,x
   rol pffb+$08,x
   ror pffb+$00,x
   dex
   bpl scroll

   lsr currentbit
   bne noscroll

   lda #$80
   sta currentbit

   ldx txtoff
@rereadtxt:
   lda text,x
   bne @notextend
   ldx #$00
   beq @rereadtxt
@notextend:
   inx
   stx txtoff

noscroll:
.ifdef SHOWTIMING
   lda #$00
   sta COLUBK
.endif
   rts
    
RODATA_SEGMENT
text:
    .byte "           Hello girls and guys, we want to invite you "
    .byte "to the Nordlicht 2019 demo party from 19th to 21st "
    .byte "of July in Bremen, Germany. "
    .byte "For more information, visit "
    .byte $22,"https://nordlicht.demoparty.info",$22,".   "
    .byte 0
