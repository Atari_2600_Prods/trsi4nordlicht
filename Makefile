######################################################################
# General-purpose makefile for compiling Atari 2600 projects.        #
# This should work for most projects without any changes.            #
# Default output is $(CURDIR).bin.  Override PROGRAM to change this. #  
######################################################################
 
PROGRAM := $(shell basename $(CURDIR)).bin
TYPE := f6
SOURCES := $(wildcard bank[0-7])
INCLUDES := .
LIBS :=
OBJDIR := obj
DEBUGDIR := $(OBJDIR)
 
LINKCFG := atari2600_$(TYPE).ld
ASFLAGS := --cpu 6502x
LDFLAGS	= -C$(LINKCFG) \
          -m $(DEBUGDIR)/$(notdir $(basename $@)).map \
          -Ln $(DEBUGDIR)/$(notdir $(basename $@)).labels -vm
 
EMULATORFLAGS := -type $(TYPE) -format pal #-debug
#EMULATORFLAGS := #-type $(TYPE) -format ntsc
 
################################################################################
 
CC            := cc65 
LD            := ld65
AS            := ca65
AR            := ar65
OD            := od65
EMULATOR      := stella
 
MKDIR         := mkdir
RM            := rm -f
RMDIR         := rm -rf
 
################################################################################

CRYSTAL_DATA = bank1/crysdata.s
BALL_DATA = bank1/balldefs.s
BALL_SIN = bank1/1sindefs.s
CHARMAP_DATA = bank0/charmap.s
GREETLIST = bank0/greetlist.inc

ofiles :=
sfiles := $(foreach dir,$(SOURCES),$(sort $(wildcard $(dir)/*.s))) tools/generatedbsc.s
incfiles := $(foreach dir,$(INCLUDES),$(notdir $(wildcard $(dir)/*.inc)))
extra_includes := $(foreach i, $(INCLUDES), -I $i)

ifneq ($findstring($(BALL_DATA),$(sfiles)),$(BALL_DATA))
sfiles += $(BALL_DATA)
endif
ifneq ($findstring($(BALL_SIN),$(sfiles)),$(BALL_SIN))
sfiles += $(BALL_SIN)
endif
ifneq ($findstring($(CRYSTAL_DATA),$(sfiles)),$(CRYSTAL_DATA))
sfiles += $(CRYSTAL_DATA)
endif
ifneq ($findstring($(GREETLIST),$(incfiles)),$(GREETLIST))
incfiles += $(GREETLIST)
endif
ifneq ($findstring($(CHARMAP_DATA),$(sfiles)),$(CHARMAP_DATA))
sfiles += $(CHARMAP_DATA)
endif

define depend
  my_obj := $$(addprefix $$(OBJDIR)/, $$(addsuffix .o65, $$(notdir $$(basename $(1)))))
  ofiles += $$(my_obj)
 
  $$(my_obj): $(1) $(incfiles) Makefile
	$$(AS) -g -o $$@ $$(ASFLAGS) $(extra_includes) $$<
endef
 
################################################################################
 
.SUFFIXES:
.PHONY: all clean run rundebug
all: $(PROGRAM)

$(foreach file,$(sort $(sfiles)),$(eval $(call depend,$(file))))

$(OBJDIR):
	[ -d $@ ] || mkdir -p $@
 
$(PROGRAM): $(OBJDIR) $(ofiles) $(LINKCFG)
	$(LD) -o $@ $(LDFLAGS) $(ofiles) $(LIBS) 

$(LINKCFG) tools/generatedbsc.s: tools/bankf8f6f4sc.sh
	$< $(TYPE)
 
run: $(PROGRAM)
	$(EMULATOR) $(EMULATORFLAGS) $(PROGRAM)
 
rundebug: $(PROGRAM)
	$(EMULATOR) -debug $(EMULATORFLAGS) $(PROGRAM)
 
clean:
	$(RM) $(ofiles) $(PROGRAM) $(LINKCFG) tools/generatedbsc.s $(CRYSTAL_DATA) $(BALL_DATA) $(BALL_SIN) $(GREETLIST) $(CHARMAP_DATA)
	$(RMDIR) $(OBJDIR)
	$(MAKE) -C tools $@

stat: $(PROGRAM)
	($(OD) -S $(ofiles)|grep -v -e ' 0$$' -e 'Segment sizes:';grep -e 'Segment list:' -e '^Name' -e '^CODE' -e '^RODATA' -e '^BSC3' $(DEBUGDIR)/$(shell basename $(CURDIR)).map)|grep -v 000000|tee $(DEBUGDIR)/stat.txt

zip: $(PROGRAM)
	rm -rf $(OBJDIR);cd ..;rm -f $(DIRNAME).zip;7z a -mx=9 $(DIRNAME).zip $(DIRNAME)

sc: $(PROGRAM)
	makewav -ts -aUSB* $(PROGRAM)
 
hd: $(PROGRAM)
	hd $<

$(CRYSTAL_DATA): tools/calccrystal
	$< >$@
 
$(CHARMAP_DATA): tools/charmap
	$< >$@
 
$(BALL_DATA): tools/calcball
	$< >$@

$(BALL_SIN): tools/calcsin
	$< >$@

$(GREETLIST): tools/greetconvert
	$< >$@
 
tools/calccrystal: tools/calccrystal.c
	$(MAKE) -C tools calccrystal

tools/calcball: tools/calcball.c
	$(MAKE) -C tools calcball

tools/calcsin: tools/calcsin.c
	$(MAKE) -C tools calcsin

tools/charmap: tools/charmap.c
	$(MAKE) -C tools charmap

tools/greetconvert: tools/greetconvert.c
	$(MAKE) -C tools greetconvert


