
; configuration parameters

.include "bankswitch.inc"

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error
.define TIMER_VBLANK   $2a ; ~2688 cycles
.define TIMER_SCREEN   $13
.define TIMER_OVERSCAN $12 ; ~1280 cycles

; all symbols visible to other source files

.global bankjmpcode0
.global bankrtscode0

; 0pglobal.s
.globalzp schedule
.globalzp localramstart

.define SCROLLDELAY 1
.define NORMALHEIGHT 6
.define MIRROREDHEIGHT 3

.global   cont_scroll
.global   scrollcharset
.global   calcscroll
.global   text
.globalzp pffb
.globalzp delay
.globalzp currentbit
.globalzp curchar
.globalzp txtoff

.globalzp temp8
.globalzp temp16
.globalzp psmkAttenuation
.globalzp psmkBeatIdx
.globalzp psmkPatternIdx
.globalzp psmkTempoCount

;;;;;;;;;
; bank0 ;
;;;;;;;;;

; main.s
.global   reset
.global   mainloop

.global   global_scroller

.global   charsetptr_low
.global   charsetptr_high
.global   scrollerstart
.global   calcscroll
.global   firstframe
.global   lastframe
.global   wait5

;;;;;;;;;
; bank1 ;
;;;;;;;;;
.global   ball
.global   ball2
.global   mul

;;;;;;;;;
; bank2 ;
;;;;;;;;;
.global   crystal
.global   greetings
.global   GFX
.global   title
.global   credits
.global   url
.global   checknext

;;;;;;;;;
; bank3 ;
;;;;;;;;;
.global   logo
.global   bigimage


; each one of these can be called: lda #DELAY : jsr x + 2
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp
.linecont +
   .define partsaddrlist \
   firstframe-1, \
   wait5-1, \
   logo-1, \
   title-1, \
   ball-1, \
   url-1, \
   crystal-1, \
   bigimage-1, \
   ball2-1, \
   greetings-1, \
   credits-1, \
   lastframe-1
.linecont -

