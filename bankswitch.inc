
.global bankjmpcode0
.global bankrtscode0

.define waitvblank   waitvblank0
.define waitscreen   waitscreen0
.define waitoverscan waitoverscan0

.macro bankjsr _addr
   ldy #<(_addr-1)
   lda #>(_addr-1)
   jsr bankjmpcode0
.endmacro

.macro bankjmp _addr
   ldy #<(_addr-1)
   lda #>(_addr-1)
   jmp bankjmpcode0
.endmacro

.macro bankrts
   jmp bankrtscode0
.endmacro
