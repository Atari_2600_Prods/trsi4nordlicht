#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void calc_ball(int size)
{
    int i;
    printf(".include \"locals.inc\"\nRODATA_SEGMENT\n");
    printf(".global ball%d\n", size);
    printf("ball%d:\n", size);
    for(i=-size; i <= size; i+=2)
    {
        double i_f = (double)i;
        double size_f = (double)size;
        double angle = asin(i_f/size_f);
        //printf("%lf\n", angle);
        //printf("%lf\n", cos(angle));
        double amp = cos(angle)*(size_f/2.0);
        if ((int)(amp+.5) != 0)
        {
            printf("    .byte %d\n", (int)(amp+.5));
        }
    }
    printf("    .byte 255\n");
}

int main()
{
    calc_ball(70);
    return 0;
}
