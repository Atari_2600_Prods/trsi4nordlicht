#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define STEPS 1

signed int calc_value(int i)
{
	double f;
	f = (double)i+82;
	f = 255.0 * sin(f*M_PI/210.0);
	return (signed int)(f+.5);
}

int main(void)
{
        printf(".include \"globals.inc\"\n");
        printf(".include \"locals.inc\"\n");
        printf("RODATA_SEGMENT\n");
	signed int a[256/STEPS];
	int i;
	int dir[256/STEPS];
	for (i=0; i < 256/STEPS; ++i)
	{
		a[i] = calc_value(i*STEPS);
		if(a[i] >= 0)
			dir[i] = 0xf0;
		else {
			dir[i] = 0x10;
			a[i] = -a[i];
		}
		//printf("%d ", a[i]);
	}
	printf("crystal_diffs:\n");
	for(i=0; i < 256/STEPS; ++i)
	{
		printf(".byte %d\n", a[i]);
	}
	printf("crystal_dirs:\n");
	for(i=0; i < 256/STEPS; ++i)
	{
		printf(".byte $%02x\n", dir[i]);
	}
	printf("crystal_sizes:\n");
	for (i=0; i < 256; ++i)
	{
		double f;
		f = (double)i+128;
		f = 6.0 * sin(f*M_PI/128.0)-6.0+28.5;
		printf(" .byte %d\n", (int)f);
	}
	printf("crystal_xpos:\n");
	for (i=0; i < 256; ++i)
	{
		double f;
		f = (double)i+128;
		f = 32.0 * sin(f*M_PI/128.0)+88.5;
		printf(" .byte %d\n", (int)f);
	}

	return 0;
}

