#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{
    int i;
    printf(".include \"locals.inc\"\n");
    printf("RODATA_SEGMENT\n");
    printf(".global sintab\n");
    printf(".align $100\n");
    printf("sintab:\n");
    for(i=0; i < 256; ++i)
    {
        double i_f = (double)i;
        double s_f = sin(i_f/128.0*M_PI) * 255.9;
        int s = (int)s_f;
        if (s < 0)
        {
            s = -s/2 + 128;
        }
        else
        {
            s = s/2;
        }
        printf("    .byte $%02x\n", s);
    }
    return 0;
}
