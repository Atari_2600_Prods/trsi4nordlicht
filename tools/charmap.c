#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf(".include   \"globals.inc\"\n");
    printf(".include   \"locals.inc\"\n");
    printf("RODATA_SEGMENT\n");
    printf(".align $100\n");
    printf("charsetptr_low:\n");
    int i;
    for(i=0; i < 128; ++i)
    {
        if (i < 0x20 || i > 0x7f)
        {
            printf(" .byte <scrollcharset\n");
        }
        else
        {
            printf(" .byte <(scrollcharset+$%02x)\n", (i-0x20)*8);
        }
    }
    printf("charsetptr_high:\n");
    for(i=0; i < 128; ++i)
    {
        if (i < 0x20 || i > 0x7f)
        {
            printf(" .byte >scrollcharset\n");
        }
        else
        {
            printf(" .byte >(scrollcharset+$%02x)\n", (i-0x20)*8);
        }
    }
    return 0;
}
