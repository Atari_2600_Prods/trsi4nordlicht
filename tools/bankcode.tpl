
.segment "BSC{BANK}"

waitvblank{BANK}:
   lda #TIMER_SCREEN
@waitloop:
   bit TIMINT
   bpl @waitloop
   sta WSYNC
   sta TIM1KTI
   rts

waitscreen{BANK}:
   lda #TIMER_OVERSCAN
@waitloop:
   bit TIMINT
   bpl @waitloop
   sta WSYNC
   sta TIM64TI
   lda #$02
   sta VBLANK
   rts
   
waitoverscan{BANK}:
   lda #TIMER_VBLANK
   pha
   ldy #<(mainloop-1)
   lda #>(mainloop-1)
   ; slip through
   
bankjmpcode{BANK}:
   pha
   tya
   pha
   ; slip through

bankrtscode{BANK}:
   tsx
   lda $02,x
   asl
   rol
   rol ; {!F8} {!F6}
   rol ; {!F8}
   and #%00000001 ; {!F4} {!F6}
   and #%00000011 ; {!F4} {!F8}
   and #%00000111 ; {!F6} {!F8}
   tax
   lda {HOTSPOTS},x
   rts

bankreset{BANK}:
   lda {HOTSPOTS} + (reset >> 15) ; {!F4} {!F6}
   lda {HOTSPOTS} + (reset >> 14) ; {!F4} {!F8}
   lda {HOTSPOTS} + (reset >> 13) ; {!F6} {!F8}
   jmp reset

.segment "VECTORS{BANK}"
   ; dummy data for hotspots
   .byte {HOTSPOTDATA}

   .word bankreset{BANK}
.if .defined(bank{BANK}brk)
   .word bank{BANK}brk
.else
   .word bankreset{BANK}
.endif

