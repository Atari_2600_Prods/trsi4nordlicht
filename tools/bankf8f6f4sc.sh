#!/bin/bash -e

mode="$(echo ${1}|tr A-Z a-z)"
case "${mode}" in
f4|f6|f8) bstype="${mode}"; ram_size=0;;
f4sc|f6sc|f8sc) bstype="${mode%sc}"; ram_size=128;;
*) echo "usage: $0 <f4|f6|f8>(sc)"; exit 0;
esac

mydir="${0%/*}"
if [ ! -d "${mydir}" ]; then
   echo "can't find directory ${mydir}"
   exit 1;
fi

TMPOBJ="tmp.o65"
GENSRC="generatedbsc.s"
LDSCRIPT="../atari2600_${mode}.ld"

cd "${mydir}"

generatebankswitchcode()
{
case "${bstype}" in
f8) hotspotdata='$f8,$f9,$00,$00'; banks='0:7 1:f';;
f6) hotspotdata='$f6,$f7,$f8,$f9,$00,$00'; banks='0:3 1:7 2:b 3:f';;
f4) hotspotdata='$f4,$f5,$f6,$f7,$f8,$f9,$fa,$fb'; banks='0:1 1:3 2:5 3:7 4:9 5:b 6:d 7:f';;
esac

cat <<EOF

; automatically generated

.include "vcs.inc"
.include "globals.inc"

EOF

for i in ${banks}; do
  bank="${i%:*}"
  sed \
      -e "/.*{!${bstype}}.*/Id" \
      -e "s/{BANK}/${bank}/g" \
      -e "s/{HOTSPOTS}/\$ff${bstype}/g" \
      -e "s/{HOTSPOTDATA}/${hotspotdata}/g" \
      -e "s/;.*$//" \
      bankcode.tpl
done
}

linkerscript()
{
ca65 -I .. -o ${TMPOBJ} ${GENSRC}
eval $(od65 -S ${TMPOBJ} | grep '^   ' | tr -d ' ' | tr : =)
rm -f ${TMPOBJ}

rom_base=61440 #F000
rom_end=65536 #FFFF+1
vectors_size=${VECTORS0}
vectors_start=$((rom_end-vectors_size))
bsc_size=${BSC0}
bsc_start=$((vectors_start-bsc_size))
bank_size=$((bsc_start-16#F000-2*ram_size))
options2='start=$%04X, size=$%02X,   type=ro, file=%%O, fill=yes'
options4='start=$%04X, size=$%04X, type=ro, file=%%O, fill=yes'
optionsd=', define=yes'
options0="${optionsd}"

cat <<EOF

# Atari 2600 VCS linker script for bankswitch type ${mode}

MEMORY {
EOF
for i in ${banks}; do
  bank="${i%:*}"
  addr="${i#*:}"
  addr="$((16#${addr}*4096+2*ram_size))"
  if [ ${ram_size} -gt 0 ]; then
    printf " RAM%XW:    ${options2}${options0};\n" ${bank} ${rom_base} ${ram_size}
    printf " RAM%XR:    ${options2}${options0};\n" ${bank} $((rom_base+ram_size)) ${ram_size}
  fi
  printf " ROM%X:     ${options4}${optionsd};\n" ${bank} ${addr} ${bank_size}
  printf " BSC%X:     ${options2}${options0};\n" ${bank} ${bsc_start} ${bsc_size}
  printf " VECTORS%X: ${options2}${options0};\n" ${bank} ${vectors_start} ${vectors_size}
  options0=''
done
cat <<EOF
 TIA:      start=\$00,   size=\$40,   type=rw, define=yes;
 RAM:      start=\$80,   size=\$80,   type=rw, define=yes;
 RIOT:     start=\$0280, size=\$20,   type=rw, define=yes;
}

SEGMENTS {
EOF

optional=''
for i in ${banks}; do
  bank="${i%:*}"
cat <<EOF
 RODATA${bank}:   load=ROM${bank},     type=ro, align=\$100, optional=yes;	
 CODE${bank}:     load=ROM${bank},     type=ro, define=yes${optional};
 BSC${bank}:      load=BSC${bank},     type=ro;
 VECTORS${bank}:  load=VECTORS${bank}, type=ro;
EOF
  optional=', optional=yes'
done

cat <<EOF
 ZEROPAGE:  load=RAM,      type=zp;
 TIA:       load=TIA,      type=rw, define=yes, optional=yes;
 RIOT:      load=RIOT,     type=rw, define=yes, optional=yes;
}

EOF
}

echo "generating bank switch code"
generatebankswitchcode > ${GENSRC}

echo "generating linker script"
linkerscript > ${LDSCRIPT}
