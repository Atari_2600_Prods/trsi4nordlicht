#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char str0[] ="     64 Ever * Abyss Connection * Akronyme Analogiker * Altair * Anubis * Aquanauts"
        " * Attention Whore * Bronx * Brutal * Byterapers * "
        "Cncd * Camelot * Censor";
const char str1[] =  "Chorus * Chrome * Cluster * Cosine * DMAgic * Danish Gold * Dekadence"
                " * Digital Sounds System * "
                "Dual Crew Shining  * Extend * Excess * "
                "Fairlight * Flush * Focus";
const char str2[] =   "Genesis Project * Gnumpf-Posse * HVSC-Crew * Hitmen-1  * Hitmen-2 * "
                "Hokuto Force * JAC! * LFT * "
                "K2 * Maniacs of Noise * MEGA * Metalvotze * Nostalgia * Onslaught";
const char str3[] =   "Oxyron * Padua * Performers * Plush * PriorArt * Rabenauge * Red Brand * Remember"
                " * Resource * Role * Rtficial * SCS+TRC"
                "Samar * Scoopex * Silicon LTD. * Singular Crew";
const char str4[] =   "   Skalaria * Smash Design * Tempest * "
                "The Dreams * The Solution * Triad * Trilobit * Titan * Tropyx"
                " * Vibrants * Viruz * Vision * "
                "Wrath design * Xayax * Xenon";

const char *str[] = {str0, str1, str2, str3, str4};

int main()
{
    int i;
    int len_max = 0;
    int len[5];
    for(i=0; i < 5; ++i)
    {
        len[i] = strlen(str[i]);
        fprintf(stderr, "str[%d] = %d\n", i, len[i]);
        if (len[i] > len_max)
        {
            len_max = len[i];
        }
    }
    fprintf(stderr, "max = %d\n", len_max);
    for(i=0; i < len_max; ++i)
    {
        int j;
        printf(" .byte \"");
        for(j=0; j < 5; ++j)
        {
            if (i >= len[j])
            {
                printf(" ");
            }
            else
            {
                printf("%c",str[j][i]);
            }
        }
        printf ("\"\n");
    }
    return 0;
}
