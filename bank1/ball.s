.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"

.import   ball70
.segment "ZEROPAGE"

c_initdone = localramstart
c_initdone_low = c_initdone+1
ycoord = c_initdone+2
ycoord_low = c_initdone+3
dycoord = c_initdone+4
dycoord_low = c_initdone+5
turn_speed = c_initdone+6
ball_x = c_initdone+7
axs_offset =  c_initdone+8
BG_COLOR  = $00

ballloop = c_initdone+9
         
RODATA_SEGMENT

        SPEED_BOUNCE = $06

ball2:
	lda c_initdone
        bne ball_noinit
        lda   #$81
        sta   axs_offset
        lda   #$08
        sta   turn_speed
        jmp   ballinit_continue

ball:
	lda c_initdone
        bne ball_noinit
        lda #$23
        sta axs_offset
        lda #$01
        sta turn_speed
ballinit_continue:
        lda #$03
        sta c_initdone
        lda #$01
        sta ycoord
        lda #$38
        sta ball_x
        ldx #ballloop_len
copymulloop:
        lda ballloop_copy,x
        sta ballloop,x
        dex
        bpl copymulloop
        lda #$00
        sta HMP1
ball_noinit:
        lda #$00
        sta COLUP0
        lda #%11000000
        sta PF0
        lda #%11111111
        sta PF1
        sta PF2
        lda #$01
        sta CTRLPF
        lda #$80
        sta GRP0

        lda   dycoord_low
        clc
        adc   #<SPEED_BOUNCE
        sta   dycoord_low
        lda   dycoord
        adc   #>SPEED_BOUNCE
        sta   dycoord
        lda   dycoord_low
        clc
        adc   ycoord_low
        sta   ycoord_low
        lda   dycoord
        adc   ycoord
        sta   ycoord
        cmp   #$14
        bcc   no_bounce

        lda   turn_speed
        eor   #$ff
        clc
        adc   #$01
        sta   turn_speed
        sec
        lda   dycoord_low
        eor   #$ff
        adc   #<($10000-SPEED_BOUNCE)
        sta   dycoord_low
        lda   dycoord
        eor   #$ff
        adc   #>($10000-SPEED_BOUNCE)
        sta   dycoord
        
        ;lda   c_initdone
        ;cmp   #$02
        ;bne   no_bounce
no_bounce:
        jsr   waitvblank
        ldx   ycoord
wait_loop:
        sta   WSYNC
        dex
        bne   wait_loop
        lda   ballloop+sintaboffset
        pha
        ldx   #0
        lda   #<ball70
        sta   ballloop+balllow_offset
        sta   WSYNC        
        jmp   ballloop
ballloop_back:
        pla
        clc
        adc   turn_speed
        sta   ballloop+sintaboffset
        sta   WSYNC
        lda   #BG_COLOR
        sta   COLUP0
        dec   c_initdone_low
        bne   no_exit
        dec   c_initdone
        bne   no_exit
        cli
no_exit:
        bankjmp global_scroller
        ;jsr   waitscreen
        ;jmp   waitoverscan

ballloop_copy:
balllow_offset = *-ballloop_copy+1
        ldy   ball70
        bmi   endball
.global sintab
sintaboffset = *-ballloop_copy+1
        lda     sintab
        asl
        bcs     ballnega
        STA     ballloop+mul1offset2     ;.MULZP2+1
        EOR     #$FF
        STA     ballloop+mul1offset4     ;.MULZP4+1
        SEC
mul1offset2 = * - ballloop_copy + 1
        LDA     multab2,Y
mul1offset4 = * - ballloop_copy + 1
        SBC     multab4,Y

        bcs   do_ball_continue

ballnega:
        STA     ballloop+mul2offset2     ;.MULZP2+1
        EOR     #$FF
        STA     ballloop+mul2offset4     ;.MULZP4+1
        clc
mul2offset4 = * - ballloop_copy + 1
        lda     multab4,Y
mul2offset2 = * - ballloop_copy + 1
        sbc     multab2,Y
        ;sec
do_ball_continue:
        jmp ball_continue
endball:jmp ballloop_back
ballloop_len = * - ballloop_copy

ball_continue:

            ;PosObject
            sta WSYNC                  ; 00     Sync to start of scanline.
            ldx   #BG_COLOR
            stx   COLUP0
            ldx   #$00
            sec                        ; 02     Set the carry flag so no borrow will be applied during the division.
            adc ball_x
divideby15: sbc #15                    ; 04     Waste the necessary amount of time dividing X-pos by 15!
            bcs divideby15            ; 06/07  11/16/21/26/31/36/41/46/51/56/61/66
            tay
            lda fineAdjustTable,y      ; 13 -> Consume 5 cycles by guaranteeing we cross a page boundary
            sta HMP0,x
            sta RESP0,x                ; 21/ 26/31/36/41/46/51/56/61/66/71 - Set the rough position.

        lax   ballloop+sintaboffset
        ldy   ball_colortab,x
        sbc   axs_offset
        sta   WSYNC
        sta   HMOVE
        sty   COLUP0
        sty   NUSIZ0
        inc   <(balllow_offset+ballloop)
        sta   ballloop+sintaboffset
        jmp   ballloop  

mul:
        ;x = x * y (highbyte)
        pla
        tax
        pla
        tay
        lda     $81
        pha
        stx     $81
        tya
        clc
        adc     $81
        tay
        lda     $81
        eor     #$ff
        sta     $81
        txa
        clc
        adc     $81
        tax
        SEC
        LDA     multab2,Y
        SBC     multab4,X
        tsx
        sta     $00,x
        pla
        sta     $81
        pla
        tay
        pla
        jmp    bankjmpcode0

