
.define RODATA_SEGMENT .segment "RODATA1"
.define CODE_SEGMENT   .segment "CODE1"


.global   crystal_dirs
.global   crystal_diffs
.global   crystal_xpos
.global   crystal_sizes

.global   PosObject
.global   ball_colortab
.global   fineAdjustTable
.global   multab2
.global   multab4
