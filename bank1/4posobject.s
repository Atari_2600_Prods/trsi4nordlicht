            .include "vcs.inc"
            .include "locals.inc"
RODATA_SEGMENT

; This table converts the "remainder" of the division by 15 (-1 to -15) to the correct
; fine adjustment value. This table is on a page boundary to guarantee the processor
; will cross a page boundary and waste a cycle in order to be at the precise position
; for a RESP0,x write
            .align $100

fineAdjustBegin:
            .byte %01110000; Left 7 
            .byte %01100000; Left 6
            .byte %01010000; Left 5
            .byte %01000000; Left 4
            .byte %00110000; Left 3
            .byte %00100000; Left 2
            .byte %00010000; Left 1
            .byte %00000000; No movement.
            .byte %11110000; Right 1
            .byte %11100000; Right 2
            .byte %11010000; Right 3
            .byte %11000000; Right 4
            .byte %10110000; Right 5
            .byte %10100000; Right 6
            .byte %10010000; Right 7

fineAdjustTable = fineAdjustBegin - %11110001; NOTE: %11110001 = -15

; Positions an object horizontally
; Inputs: A = Desired position.
; X = Desired object to be positioned (0-4).
;0 = Player0
;1 = Player1
;2 = Missile0
;3 = Missile1
;4 = Ball
; scanlines: If control comes on or before cycle 73 then 1 scanline is consumed.
; If control comes after cycle 73 then 2 scanlines are consumed.
; Outputs: X = unchanged

; A = Fine Adjustment value.
; Y = the "remainder" of the division by 15 minus an additional 15.
; control is returned on cycle 6 of the next scanline.

PosObject:
            sta WSYNC                  ; 00     Sync to start of scanline.
            sec                        ; 02     Set the carry flag so no borrow will be applied during the division.
divideby15_2: sbc #15                    ; 04     Waste the necessary amount of time dividing X-pos by 15!
            bcs divideby15_2            ; 06/07  11/16/21/26/31/36/41/46/51/56/61/66
            tay
            lda fineAdjustTable,y      ; 13 -> Consume 5 cycles by guaranteeing we cross a page boundary
            sta HMP0,x
            sta RESP0,x                ; 21/ 26/31/36/41/46/51/56/61/66/71 - Set the rough position.
            rts 
