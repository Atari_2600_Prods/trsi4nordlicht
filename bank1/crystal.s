
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.segment "ZEROPAGE"

CRYSTAL_COLOR = $ce
CRYSTAL_COLOR2 = $c6


c_initdone = localramstart
c_initdone_low =  c_initdone+1
c_addvalue = c_initdone+2
c_base_x =   c_initdone+3
c_x_start =  c_initdone+4

C_x_diff1 =   c_initdone+5
C_x_val1  =   c_initdone+6
C_x_dir1  =   c_initdone+7

C_x_diff2  =   c_initdone+8
C_x_val2  =   c_initdone+9
C_x_dir2  =   c_initdone+10

C_x_diff3  =   c_initdone+11
C_x_val3  =   c_initdone+12
C_x_dir3  =   c_initdone+13

C_x_diff4  =   c_initdone+14
C_x_val4  =   c_initdone+15
C_x_dir4  =   c_initdone+16

C_x_diff5  =   c_initdone+17
C_x_val5  =   c_initdone+18
C_x_dir5  =   c_initdone+19
c_index  =   c_initdone+20
c_size  =   c_initdone+21
c_size_1  =   c_initdone+22
c_size_index  =   c_initdone+23
c_delay  =   c_initdone+24
c_pos_index  =   c_initdone+25


RODATA_SEGMENT

crystal:
	lda c_initdone
        bne crystal_noinit
	lda #$03
	sta c_initdone
	lda #$00
	sta c_pos_index
	sta c_size_index
	sta c_index
	sta c_base_x
	lda #$58
	sta c_x_start
	lda #$04
	sta CTRLPF
	ldx #$1c
	stx c_size
crystal_noinit:
        jsr   waitvblank
	lda #$00
	sta COLUP0
	sta COLUP1
	sta COLUPF

	ldy c_size_index
	ldx crystal_sizes,y
	stx c_size
	inx
	stx c_size_1
	lda #$1c
	sec
	sbc c_size
	sta c_delay
	inc c_size_index

	ldx c_pos_index
	lda crystal_xpos,x
	sta c_x_start
	lda c_size_index
	lsr
	bcc l_crystal1
	inc c_pos_index
l_crystal1:
        sta WSYNC
	inc c_pos_index
	lax c_index
	ldy crystal_diffs,x
	sty C_x_diff5
	ldy crystal_dirs,x
	sty C_x_dir5
	axs #51
	ldy crystal_diffs,x
	sty C_x_diff1
	ldy crystal_dirs,x
	sty C_x_dir1
	txa
	axs #51
	ldy crystal_diffs,x
	sty C_x_diff2
	ldy crystal_dirs,x
	sty C_x_dir2
	txa
	axs #51
	ldy crystal_diffs,x
	sty C_x_diff3
	ldy crystal_dirs,x
	sty C_x_dir3
	txa
	axs #51
	ldy crystal_diffs,x
	sty C_x_diff4
	ldy crystal_dirs,x
	sty C_x_dir4

        sta WSYNC
	dec c_index
	dec c_index
	lda c_index
	cmp #52
	bne l_crystal2
	lda #$00
	sta c_index
l_crystal2:
        sta WSYNC
	lda #$80
	sta C_x_val1
	sta C_x_val2
	sta C_x_val3
	sta C_x_val4
	sta C_x_val5

	lda #$f0
	sta c_addvalue

        ldx #$00
        lda c_x_start
        jsr PosObject
        sta WSYNC

        ldx #$01
        lda c_x_start
        jsr PosObject
        sta WSYNC

        ldx #$02
        lda c_x_start
	clc
	adc #$01
        jsr PosObject
        sta WSYNC

        ldx #$03
        lda c_x_start
	clc
	adc #$01
        jsr PosObject
        sta WSYNC

        ldx #$04
        lda c_x_start
	clc
	adc #$01
        jsr PosObject
        sta WSYNC


	lda #$80
	sta GRP0
	sta GRP1
	lda #$02
	sta ENAM0
	sta ENAM1
	sta ENABL
	lda #$00
	sta COLUBK
	sta WSYNC
	sta HMOVE
	;jsr WaitForVblankEnd
	;sta HMCLR
	ldx c_delay
	beq l_crystal3
l_crystal4:
	sta WSYNC
	dex
	bne l_crystal4
l_crystal3:
	sta WSYNC

	nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop

	nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop

	nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop

	nop
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        
	lda #CRYSTAL_COLOR
	sta COLUP1
	sta COLUP0
	sta COLUPF
	ldx c_size
	jsr paint_crystal
	jsr invert_crystal
	sta WSYNC
	lda #CRYSTAL_COLOR2
	sta COLUP1
	sta COLUP0
	sta COLUPF
	ldx c_size_1
	jsr paint_crystal
	lda #$00
	sta GRP0
	sta GRP1
	sta ENAM0
	sta ENAM1
	sta ENABL
	jsr invert_crystal

;l_crystal_5:
;        sta WSYNC
;	lda INTIM
;	cmp #$06
;	bne l_crystal_5
;        sta WSYNC
	ldx c_base_x
	lda #$80
	pha
l_crystal_6:
	pla
	clc
	adc c_addvalue
	bcc l_crystal_7
	inx
	clc
l_crystal_7:
	adc c_addvalue
	dec c_addvalue
	bcc l_crystal_8
	inx
	clc
l_crystal_8:
	adc c_addvalue
	dec c_addvalue
	bcc l_crystal_9
	inx
	clc
l_crystal_9:
        ;sta WSYNC
	;pha
	;lda base_colortab,x
        ;lda $0000,x
	;sta WSYNC
	;sta COLUBK
	;lda INTIM	
	;bne l_crystal_6
	;sta WSYNC

        ;lda   #$00
        ;sta   COLUBK
        dec   c_initdone_low
        bne   no_exit
        dec   c_initdone
        bne   no_exit
        cli
no_exit:
	dec c_base_x
        bankjmp global_scroller
        ;jsr   waitscreen
        ;jmp   waitoverscan

invert_crystal:
	lda C_x_dir1
	eor #$e0
	sta C_x_dir1
	lda C_x_dir2
	eor #$e0
	sta C_x_dir2
	lda C_x_dir3
	eor #$e0
	sta C_x_dir3
	lda C_x_dir4
	eor #$e0
	sta C_x_dir4
	lda C_x_dir5
	eor #$e0
	sta C_x_dir5
	lda #$00
	sec
	sbc C_x_val1
	sta C_x_val1
	lda #$00
	sec
	sbc C_x_val2
	sta C_x_val2
	lda #$00
	sec
	sbc C_x_val3
	sta C_x_val3
	lda #$00
	sec
	sbc C_x_val4
	sta C_x_val4
	lda #$00
	sec
	sbc C_x_val5
	sta C_x_val5
	rts

paint_crystal:
l_crystal_11:
	lda C_x_val1
	clc
	adc C_x_diff1
	sta C_x_val1
	lda C_x_dir1
	ldy #$08
l_crystal_10:
	dey
	bne l_crystal_10
	sta HMCLR
	bcc l_crystal_12
	sta HMP0
l_crystal_12:
	lda C_x_val2
	clc
	adc C_x_diff2
	sta C_x_val2
	lda C_x_dir2
	bcc l_crystal_13
	sta HMM0
l_crystal_13:
	lda C_x_val3
	clc
	adc C_x_diff3
	sta C_x_val3
	lda C_x_dir3
	bcc l_crystal_14
	sta HMP1
l_crystal_14:
	lda C_x_val4
	clc
	adc C_x_diff4
	sta C_x_val4
	lda C_x_dir4
	bcc l_crystal_15
	sta HMM1
l_crystal_15:
	lda C_x_val5
	clc
	adc C_x_diff5
	sta C_x_val5
	lda C_x_dir5
	bcc l_crystal_16
	sta HMBL
l_crystal_16:
	sta WSYNC
	sta HMOVE
	dex
	bne l_crystal_11
kerneldone:
        rts

            .if 0
; This table converts the "remainder" of the division by 15 (-1 to -15) to the correct
; fine adjustment value. This table is on a page boundary to guarantee the processor
; will cross a page boundary and waste a cycle in order to be at the precise position
; for a RESP0,x write
            .align $100

fineAdjustBegin:
            .byte %01110000; Left 7 
            .byte %01100000; Left 6
            .byte %01010000; Left 5
            .byte %01000000; Left 4
            .byte %00110000; Left 3
            .byte %00100000; Left 2
            .byte %00010000; Left 1
            .byte %00000000; No movement.
            .byte %11110000; Right 1
            .byte %11100000; Right 2
            .byte %11010000; Right 3
            .byte %11000000; Right 4
            .byte %10110000; Right 5
            .byte %10100000; Right 6
            .byte %10010000; Right 7

fineAdjustTable = fineAdjustBegin - %11110001; NOTE: %11110001 = -15


; Positions an object horizontally
; Inputs: A = Desired position.
; X = Desired object to be positioned (0-4).
;0 = Player0
;1 = Player1
;2 = Missile0
;3 = Missile1
;4 = Ball
; scanlines: If control comes on or before cycle 73 then 1 scanline is consumed.
; If control comes after cycle 73 then 2 scanlines are consumed.
; Outputs: X = unchanged

; A = Fine Adjustment value.
; Y = the "remainder" of the division by 15 minus an additional 15.
; control is returned on cycle 6 of the next scanline.

PosObject:
            sta WSYNC                  ; 00     Sync to start of scanline.
            sec                        ; 02     Set the carry flag so no borrow will be applied during the division.
divideby15: sbc #15                    ; 04     Waste the necessary amount of time dividing X-pos by 15!
            bcs divideby15            ; 06/07  11/16/21/26/31/36/41/46/51/56/61/66
            tay
            lda fineAdjustTable,y      ; 13 -> Consume 5 cycles by guaranteeing we cross a page boundary
            sta HMP0,x
            sta RESP0,x                ; 21/ 26/31/36/41/46/51/56/61/66/71 - Set the rough position.
            rts 
            .endif
