
.include "globals.inc"
.include "locals.inc"

CODE_SEGMENT

checknext:
   lda   psmkTempoCount
   ora   psmkBeatIdx
   bne   @done

   lda   psmkPatternIdx
   beq   @next
   cmp   #$0c
   beq   @next
   cmp   #$1c
   beq   @next
   cmp   #$32
   bne   @done

@next:
   cli

@done:
   bankrts
