
	.align 256

logo_datalines:
.byte $3c, $3c, $0f, $00
.byte $3e, $7c, $1f, $08
.byte $03, $cc, $30, $0c
.byte $03, $4c, $38, $0c
.byte $03, $7c, $3c, $0c
.byte $03, $3c, $1c, $0c
.byte $03, $7c, $1e, $0c
.byte $03, $7c, $0e, $0c
logo_datalines_end:
.byte $03, $7c, $06, $0c
.byte $03, $cc, $03, $0c
.byte $03, $cc, $3e, $04
.byte $03, $cc, $3c, $00
logo_indizes:
.byte 0*4 + LOFFSET
.byte 0*4 + LOFFSET
.byte 0*4 + LOFFSET
.byte 0*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 1*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 2*4 + LOFFSET
.byte 3*4 + LOFFSET
.byte 3*4 + LOFFSET
.byte 4*4 + LOFFSET
.byte 4*4 + LOFFSET
.byte 5*4 + LOFFSET
.byte 5*4 + LOFFSET
.byte 5*4 + LOFFSET
.byte 5*4 + LOFFSET
.byte 6*4 + LOFFSET
.byte 7*4 + LOFFSET
.byte 7*4 + LOFFSET
logo_indizes_end1:
.byte 8*4 + LOFFSET
logo_indizes_end2:
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
.byte 9*4 + LOFFSET
logo_indizes_end3:
.byte 10*4 + LOFFSET
.byte 10*4 + LOFFSET
.byte 10*4 + LOFFSET
.byte 10*4 + LOFFSET
.byte 10*4 + LOFFSET
.byte 10*4 + LOFFSET
.byte 10*4 + LOFFSET
.byte 10*4 + LOFFSET
logo_indizes_end4:
.byte 11*4 + LOFFSET
.byte 11*4 + LOFFSET
.byte 11*4 + LOFFSET
.byte 11*4 + LOFFSET
logo_palette:
.byte $20
.byte $20
.byte $22
.byte $20
.byte $22
.byte $22
.byte $24
.byte $22
.byte $24
.byte $24
.byte $48
.byte $24
.byte $48
.byte $48
.byte $4a
.byte $48
.byte $4a
.byte $4a
.byte $4c
.byte $4a
.byte $4c
.byte $4c
.byte $4e
.byte $4c
.byte $4e
.byte $4e
.byte $2e
.byte $4e
.byte $2e
.byte $2e
.byte $0e
.byte $2e
.byte $0e
.byte $0e
.byte $0e
.byte $0e
.byte $0e
.byte $0e
.byte $0e
.byte $0e
.byte $0e
.byte $0e
.byte $7e
.byte $7e
.byte $7c
.byte $7c
.byte $7a
.byte $7a
.byte $78
.byte $78
.byte $76
.byte $76
.byte $74
.byte $74
.byte $72
.byte $72
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
.byte $70
