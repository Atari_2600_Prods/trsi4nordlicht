
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.segment "ZEROPAGE"


endpos     = localramstart+ 0
xpos       = localramstart+ 1
sinbase    = localramstart+ 2
index      = localramstart+ 3
counter    = localramstart+ 4
temp       = temp8

movtab     = $c8 ; size: 48 bytes, assuming 8 bytes of stack at most



RODATA_SEGMENT


.align $100
.include "slides.inc"

;         -7  -6  -5  -4  -3  -2  -1   0  +1  +2  +3  +4  +5  +6  +7

.align $10
xpostab:
   .byte $f9,$fa,$fb,$fc,$fd,$fe,$ff,$00,$01,$02,$03,$04,$05,$06,$07
.align $10
hmpxtab:
   .byte $70,$60,$50,$40,$30,$20,$10,$00,$f0,$e0,$d0,$c0,$b0,$a0,$90

CODE_SEGMENT

title:
   lda   #$2f
   ldx   #$60
   bne   setmtp
url:
   lda   #$5f
   ldx   #$80
   bne   setmtp
credits:
   lda   #$ff
   ldx   #$30

setmtp:
   sta   endpos
   stx   index
   sec
   lda   #<movtab
   sbc   index
   adc   #$2f
   sta   temp16+0
   lda   #$00        ; movtab is in zeropage
   sta   temp16+1
   sta   GRP0
   sta   GRP1

snake48:
   lda   movtab
   bne   @noinit
   
   ldx   #$2f
   lda   #$07
:
   sta   movtab,x
   dex
   bpl   :-

   
@noinit:
   lda   counter
   beq   :+
   bankjsr checknext
:
   inc   sinbase
   bne   :+
   inc   counter
:
   ldy   sinbase
   ldx   #$2f
   lda   sinustab111,y
   sta   xpos
:
   lda   #$07
   sec
   sbc   sinustab111,y
   iny
   clc
   adc   sinustab111,y
   sta   movtab,x
   dex
   bpl   :-

   jsr   waitvblank

   sta   WSYNC      ;      0
   lda   xpos|$100  ;   4= 4
   sec              ;   2= 6
@sprloop:
   sbc   #$0f       ; x*2= 8
   bcs   @sprloop   ; x*3=11 (-1=9/44) ; maximum of 7 iterations * 5 cycles
   eor   #$07       ;   2=12/46
   asl              ;   2=14/48
   asl              ;   2=16/50
   asl              ;   2=18/52
   asl              ;   2=20/54
   sta   RESP0      ;   3=23/57
   sta   RESP1      ;   3=26/60
   sta   HMP0       ;   3=29/63
   adc   #$10       ;   2=31/65
   sta   HMP1       ;   3=34/68

   sta   WSYNC

.if 0
   lda   bgcol
   sta   COLUBK
.endif
   
   lda   #$03
   sta   NUSIZ0
   sta   NUSIZ1
   sta   VDELP0
   sta   VDELP1

   sec
   jmp   kernelstart

kernel:
   ldy   index | $100;  3= 3
   lax   (temp16),y  ;  5= 8

   lda   hmpxtab,x   ;  4=23
   sta   HMP0        ;  3=26
   sta   HMP1        ;  3=29

   ;clc               ;  2= 9 ; not needed due to modified "@waste1d"

   lda   xpos        ;  3=12
   adc   xpostab,x   ;  4=16
   sta   xpos        ;  3=19
   
   lda   name0,y     ;  4= 4
   sta   GRP0        ;  3= 7
   lda   name1,y     ;  4=11
   sta   GRP1        ;  3=14
   lda   name2,y     ;  4=18
   sta   GRP0        ;  3=21
   lax   name3,y     ;  4=25
   lda   name4,y     ;  4=29
   ldy   temp        ;  3=32

   stx   GRP1        ;  3= 3 !! FIXED POS !!
   sta   GRP0        ;  3= 6
   sty   GRP1        ;  3= 9
   sta   GRP0        ;  3=12

kernelstart:
   sta   WSYNC       ;  3=75/0
   sta   HMOVE       ;  3= 3

   lda   #$00        ;  2= 2
   sta   COLUP0      ;  3= 5
   sta   COLUP1      ;  3= 8

   lda   endpos      ;  3= 3
   dcp   index       ;  5= 8
   beq   kerneldone  ;  2=10

   ldy   index       ;  3= 3

   lda   name5,y     ;  4= 4
   sta   temp        ;  3= 7

   lda   xpos        ;  3= 3

   sec               ;  2= 2
@cycloop:
   sbc   #$0f        ;  2= 4
   bcs   @cycloop    ;  2= 6
   cmp   #$f4        ;  2= 8
   bcs   @waste1a    ;  2=10
@waste1a:
   cmp   #$f7        ;  2=12
   bcs   @waste1b    ;  2=14
@waste1b:
   cmp   #$fa        ;  2=16
   bcs   @waste1c    ;  2=18
@waste1c:
   cmp   #$fd        ;  2=20
.if 0
   bcs   @waste1d    ;  2=22 (+111/3 = 37)
.else
   bcc   @waste1d    ;  same logic as above, costs 1 cycle extra, but
   clc               ;  ensures C=0. 1 cycle less than unconditional clc
.endif
@waste1d:

   lda   name_c,y    ;  4= 4
   sta   COLUP0      ;  3= 7
   sta   COLUP1      ;  3=10

   lda   name0,y     ;  4= 4
   sta   GRP0        ;  3= 7
   lda   name1,y     ;  4=11
   sta   GRP1        ;  3=14
   lda   name2,y     ;  4=18
   sta   GRP0        ;  3=21
   lax   name3,y     ;  4=25
   lda   name4,y     ;  4=29
   ldy   temp        ;  3=32

   stx   GRP1        ;  3= 3 !! FIXED POS !!
   sta   GRP0        ;  3= 6
   sty   GRP1        ;  3= 9
   sta   GRP0        ;  3=12

   jmp   kernel      ;  3=10
kerneldone:
   lda   #$00
   sta   VDELP0
   sta   VDELP1

   bankjmp global_scroller
   ;jsr   waitscreen
   ;jmp   waitoverscan

.assert <name0 < $a1, warning, "name0 must not cross page"
.assert <name1 < $a1, warning, "name1 must not cross page"
.assert <name2 < $a1, warning, "name2 must not cross page"
.assert <name3 < $a1, warning, "name3 must not cross page"
.assert <name4 < $a1, warning, "name4 must not cross page"
.assert <name5 < $a1, warning, "name5 must not cross page"
.assert <name_c < $a1, warning, "name_c must not cross page"
