        .include "vcs.inc"
        .include "globals.inc"
        .include "locals.inc"

        c_initdone = localramstart
        c_initdone_high = c_initdone+1
	logo_stackmerk =c_initdone+2
        logo_x_swing = c_initdone+3
        logo_ypos = c_initdone+4
	logo_gfx_stack = c_initdone+5

	LOFFSET = logo_gfx_stack-1

YPOS = $20
       
	;!for i, $43 {
	;!byte 0
	;}

RODATA_SEGMENT

; This table converts the "remainder" of the division by 15 (-1 to -15) to the correct
; fine adjustment value. This table is on a page boundary to guarantee the processor
; will cross a page boundary and waste a cycle in order to be at the precise position
; for a RESP0,x write
            .align $100

fineAdjustBegin:
            .byte %01110000; Left 7 
            .byte %01100000; Left 6
            .byte %01010000; Left 5
            .byte %01000000; Left 4
            .byte %00110000; Left 3
            .byte %00100000; Left 2
            .byte %00010000; Left 1
            .byte %00000000; No movement.
            .byte %11110000; Right 1
            .byte %11100000; Right 2
            .byte %11010000; Right 3
            .byte %11000000; Right 4
            .byte %10110000; Right 5
            .byte %10100000; Right 6
            .byte %10010000; Right 7

fineAdjustTable = fineAdjustBegin - %11110001; NOTE: %11110001 = -15


; Positions an object horizontally
; Inputs: A = Desired position.
; X = Desired object to be positioned (0-4).
;0 = Player0
;1 = Player1
;2 = Missile0
;3 = Missile1
;4 = Ball
; scanlines: If control comes on or before cycle 73 then 1 scanline is consumed.
; If control comes after cycle 73 then 2 scanlines are consumed.
; Outputs: X = unchanged

; A = Fine Adjustment value.
; Y = the "remainder" of the division by 15 minus an additional 15.
; control is returned on cycle 6 of the next scanline.

PosObject:
            sta WSYNC                  ; 00     Sync to start of scanline.
            sec                        ; 02     Set the carry flag so no borrow will be applied during the division.
divideby15: sbc #15                    ; 04     Waste the necessary amount of time dividing X-pos by 15!
            bcs divideby15            ; 06/07  11/16/21/26/31/36/41/46/51/56/61/66
            tay
            lda fineAdjustTable,y      ; 13 -> Consume 5 cycles by guaranteeing we cross a page boundary
            sta HMP0,x
            sta RESP0,x                ; 21/ 26/31/36/41/46/51/56/61/66/71 - Set the rough position.
            rts 

        ;low byte of "logo" must be $43

logo:
	lda c_initdone_high
	bne logo_noinit
	lda #$03
	sta c_initdone_high
        inc c_initdone
logo_noinit:
        lda #>(logo_noinit_return-1)
        nop  ;pha
        lda #<(logo_noinit_return-1)
        nop  ;pha
        lda #$80
        nop ;pha
        ldy logo_x_swing
        lda sinustab111,y
        nop ;pha
        nop
        nop
        nop
        nop
        nop
        nop
        nop
        ;bankjmp mul
logo_noinit_return:
        nop   ;tsx
        nop  ;lda $fd,x
        lsr
        sta logo_ypos
        lda c_initdone_high
        cmp #$03
        bcs no_incswing
        inc logo_x_swing
        inc logo_x_swing
no_incswing:
	lda #$00
        sta COLUBK
	ldx #logo_datalines_end-logo_datalines-4
	ldy #logo_datalines_end-logo_datalines-1
@logoloop:
	lda logo_datalines,y
	sta logo_gfx_stack+3,x
	dey
	lda logo_datalines,y
	sta logo_gfx_stack+2,x
	dey
	lda logo_datalines,y
	sta logo_gfx_stack+1,x
	dey
	lda logo_datalines,y
	sta logo_gfx_stack+0,x
	txa
	axs #$04
	dey
	bpl @logoloop

	lda #$00
	sta PF0
	sta PF1
	sta PF2
	lda #$13
	sta CTRLPF
	tsx
	stx logo_stackmerk
	ldx #$00
	lda #$2f
	jsr PosObject
	ldx #$01
	lda #$5f
	jsr PosObject
        ldx #$02
        lda #$2e
        jsr PosObject
        ldx #$03
        lda #$4f
        jsr PosObject
        ldx #$04
        lda #$68
        jsr PosObject
	sta WSYNC
	sta HMOVE
	sta WSYNC
	sta HMCLR
	lda #$ff
	sta COLUP0
	sta COLUP1
	lda #$02
	sta NUSIZ0
        sta NUSIZ1
        lda #$48
        sta COLUPF
	jsr waitvblank
	ldx logo_ypos
@l0:	sta WSYNC
	dex
	bpl @l0
	ldy #$00
	sta WSYNC
	ldx #14
@l1:	dex
	bne @l1
	.byte $2c
@l2:
	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
	ldx logo_indizes,y
	txs
	pla
	sta PF1
	pla
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	pla
	sta PF2
	pla
	.byte $8d, PF1, 0 ; sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #logo_indizes_end1-logo_indizes ; l_registers1_end-l_registers1
	bne @l2

@l3:	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
        nop
        nop
        nop
        nop
	nop
	lda #$03
	sta PF1
	lda #$7c
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	.byte $9d, 0, 0 ;sta+2 $00,x
	lda #$06
	nop
	sta PF2
	lda #$0c
	nop
	.byte $8d, PF1, 0 ; sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	nop
	bit $ea
	;cpy #logo_indizes_end2-logo_indizes ; l_registers1_end-l_registers1
	;bne @l3

@l4:	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
        nop
        nop
        nop
	nop
	nop
	lda #$03
	sta PF1
	lda #$cc
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	lda #$03
	nop
	sta PF2
	lda #$0c
	nop
	.byte $8d, PF1, $00 ; sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #logo_indizes_end3-logo_indizes ; l_registers1_end-l_registers1
	bne @l4
	sta $00,x
	nop
	cmp (localramstart,x)
	jmp @l6
	

@l5:	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
@l6:	nop
	nop
        nop
        nop
        nop
	lda #$03
	sta PF1
	lda #$cc
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	lda #$3e
	nop
	sta PF2
	lda #$04
	nop
	.byte $8d, PF1, 0 ; sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #logo_indizes_end4-logo_indizes ; l_registers1_end-l_registers1
	bne @l5
	sta $00,x
	nop
	cmp (localramstart,x)
	jmp @l8

@l7:	sta $00,x
	lda logo_palette,y
	sta COLUP0
	sta COLUP1
@l8:	nop
	nop
	nop
	nop
	nop
	lda #$03
	sta PF1
	lda #$cc
	sta PF2
	ldx l_registers1,y
	lda l_data1,y
	sta $00,x
	lda #$3c
	nop
	sta PF2
	lda #$00
	nop
	.byte $8d, PF1, 0 ; sta+2 PF1
	ldx l_registers2,y
	lda l_data2,y
	iny
	cpy #(l_registers1_end-l_registers1)
	bne @l7
	sta $00,x        
	ldx logo_stackmerk
	txs
	lda #$00
	sta GRP0
	sta GRP1
        sta ENAM0
        sta ENAM1
        sta ENABL
        sta PF0
        sta PF1
        sta PF2
	sta COLUPF
	sta COLUP0
	sta COLUP1
        inc   c_initdone
        bne   @l10
        dec   c_initdone_high
        bne   @l10
        cli
@l10: 
        bankjmp global_scroller
        ;jsr   waitscreen
        ;jmp   waitoverscan
        ;}

        .include "picture.inc"


	.align 256
l_registers1:
        .incbin "acme_gfx.bininc"
l_registers1_end = $49+l_registers1              

l_data1 = l_registers1+$80
l_registers2 = l_registers1+$100
l_data2 = l_registers1+$180

