        !to "acme_gfx.bininc",plain
        * = 0

VSYNC  = $00 ; ---- --1- This address controls vertical sync time by writing D1 into the VSYNC latch.
VBLANK = $01 ; 76-- --1- 1=Start VBLANK, 6=Enable INPT4, INPT5 latches, 7=Dump INPT1,2,3,6 to ground
WSYNC  = $02 ; ---- ---- This address halts microprocessor by clearing RDY latch to zero. RDY is set true again by the leading edge of horizontal blank.
RSYNC  = $03 ; ---- ---- This address resets the horizontal sync counter to define the beginning of horizontal blank time, and is used in chip testing. 
NUSIZ0 = $04 ; --54 -210 \ 0,1,2: player copys'n'size, 4,5: missile size: 2^x pixels
NUSIZ1 = $05 ; --54 -210 / 0,1,2: 0:R, 1:RR, 2:R R, 3:RRR, 4:R   R, 5:LL, 6:R R R, 7:LLLL
COLUP0 = $06 ; 7654 321- color player 0
COLUP1 = $07 ; 7654 321- color player 1
COLUPF = $08 ; 7654 321- color playfield
COLUBK = $09 ; 7654 321- color background
CTRLPF = $0A ; --54 -210 0=reflect playfield, 1=pf uses player colors, 2=playfield over sprites 4,5=ballsize:2^x
REFP0  = $0B ; ---- 3--- reflect player 0
REFP1  = $0C ; ---- 3--- reflect player 1
PF0    = $0D ; DCBA ---- \   Playfield bits: ABCDEFGHIJKLMNOPQRST
PF1    = $0E ; EFGH IJKL  >  normal:  ABCDEFGHIJKLMNOPQRSTABCDEFGHIJKLMNOPQRST
PF2    = $0F ; TSRQ PONM /   reflect: ABCDEFGHIJKLMNOPQRSTTSRQPONMLKJIHGFEDCBA
RESP0  = $10 ; ---- ---- \
RESP1  = $11 ; ---- ----  \
RESM0  = $12 ; ---- ----   > reset players, missiles and the ball. The object will begin its serial graphics at the time of a horizontal line at which the reset address occurs.
RESM1  = $13 ; ---- ----  /
RESBL  = $14 ; ---- ---- /
AUDC0  = $15 ; ---- 3210 audio control voice 0
AUDC1  = $16 ; ---- 3210 audio control voice 1
AUDF0  = $17 ; ---4 3210 frequency divider voice 0
AUDF1  = $18 ; ---4 3210 frequency divider voice 1
AUDV0  = $19 ; ---- 3210 audio volume voice 0
AUDV1  = $1A ; ---- 3210 audio volume voice 1
GRP0   = $1B ; 7654 3210 graphics player 0
GRP1   = $1C ; 7654 3210 graphics player 1
ENAM0  = $1D ; ---- --1- enable missile 0
ENAM1  = $1E ; ---- --1- enable missile 1
ENABL  = $1F ; ---- --1- enable ball
HMP0   = $20 ; 7654 ---- write data (horizontal motion values) into the horizontal motion registers
HMP1   = $21 ; 7654 ---- write data (horizontal motion values) into the horizontal motion registers
HMM0   = $22 ; 7654 ---- write data (horizontal motion values) into the horizontal motion registers
HMM1   = $23 ; 7654 ---- write data (horizontal motion values) into the horizontal motion registers
HMBL   = $24 ; 7654 ---- write data (horizontal motion values) into the horizontal motion registers
VDELP0 = $25 ; ---- ---0 delay player 0 by one vertical line or until GRP1 is triggered
VDELP1 = $26 ; ---- ---0 delay player 1 by one vertical line or until GRP0 is triggered
VDELBL = $27 ; ---- ---0 delay ball by one vertical line
RESMP0 = $28 ; ---- --1- keep missile 0 aligned with player 0
RESMP1 = $29 ; ---- --1- keep missile 1 aligned with player 1
HMOVE  = $2A ; ---- ---- This address causes the horizontal motion register values to be acted upon during the horizontal blank time in which it occurs.
HMCLR  = $2B ; ---- ---- This address clears all horizontal motion registers to zero (no motion).
CXCLR  = $2C ; ---- ---- clears all collision latches

L_NOP = $2f

        !macro logo_poke .x, .y, .a, .b {
        !byte .x
        * = *+$7f
        !byte .y
        * = *+$7f
        !byte .a
        * = *+$7f
        !byte .b
        * = *-$180
        }

	+logo_poke L_NOP, 0, GRP0, $c0
	+logo_poke GRP1, $03, GRP0, $e0
	+logo_poke GRP1, $07, GRP0, $f0
	+logo_poke GRP1, $0f, GRP0, $08
	+logo_poke GRP1, $10, ENABL, $02
	+logo_poke ENAM0, $02, GRP0, $0c
	+logo_poke GRP1, $30, HMM0, $f0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, GRP0, $0e
	+logo_poke GRP1, $70, L_NOP, 0
	+logo_poke NUSIZ0, $12, L_NOP, 0
	+logo_poke HMP0, $80, GRP0, $00
	+logo_poke GRP1, 0,  HMOVE, 0
        +logo_poke CTRLPF, $03, L_NOP, 0 
        +logo_poke COLUPF, $4a, L_NOP, 0 
        +logo_poke COLUPF, $48, HMOVE, 0
	+logo_poke ENABL, 0, HMCLR, 0
	+logo_poke NUSIZ1, $10, NUSIZ0, $10
	+logo_poke HMP0, $80, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke HMP0, 0, HMOVE, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke COLUPF, $2e, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke ENABL, 2, L_NOP, 0
	+logo_poke GRP0, $0e, ENAM1, 2
	+logo_poke HMM1, $10, CTRLPF, $13 
	+logo_poke COLUPF, $4e, HMBL, $c0
	+logo_poke COLUPF, $2e, HMOVE, 0
	+logo_poke L_NOP, 0, ENAM1, 0
	+logo_poke ENABL, 0, GRP1, $70
	+logo_poke GRP0, $08, NUSIZ0, $12
	+logo_poke GRP0, $c0, HMCLR, 0
	+logo_poke GRP0, $f0, GRP1, $30
 	+logo_poke GRP0, $f8, GRP1, $10
 	+logo_poke GRP0, $fc, GRP1, $07
 	+logo_poke GRP0, $fe, L_NOP, 0
	+logo_poke L_NOP, 0, GRP1, $00
 	+logo_poke GRP0, $ff, NUSIZ1, $12
	+logo_poke HMM1, $f0, ENAM1, 2
	+logo_poke L_NOP, 0, HMOVE, 0
 	+logo_poke GRP0, $00, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke ENAM1, 0, HMM1, $80
	+logo_poke NUSIZ1, $10, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, HMM1, 0
	+logo_poke HMP1, $80, HMOVE, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke HMM1, $10, L_NOP, 0
	+logo_poke HMP1, 0, L_NOP, 0
	+logo_poke ENAM1, 2, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, HMOVE, 0
	+logo_poke GRP1, $fe, NUSIZ1, $11
	+logo_poke ENAM1, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke GRP1, $fc, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke GRP1, $f8, L_NOP, 0
	+logo_poke L_NOP, 0, L_NOP, 0
	+logo_poke GRP1, $f0, L_NOP, 0
	+logo_poke GRP1, $e0, L_NOP, 0
	+logo_poke GRP1, $c0, L_NOP, 0
	+logo_poke GRP1, $00, L_NOP, 0
