
.include "globals.inc"
.include "locals.inc"
.include "vcs.inc"

.segment "ZEROPAGE"

fgcolor  = temp8
mbgcolor = temp16+0
mfgcolor = temp16+1

RODATA_SEGMENT

powlist:
   .byte $80,$40,$20,$10,$08,$04,$02,$01

fgcolors:
   .byte $8c,$8c,$8c,$4a,$6c,$cc,$7a,$3a,$58,$dc,$7a,$7a
mfgcolors:
   .byte $00,$00,$a6,$86,$66,$86,$a6,$36,$54,$74,$a6,$a6
mbgcolors:
   .byte $00,$00,$b2,$a2,$40,$e2,$62,$22,$30,$50,$62,$62


.define TABLECOLOR $D0
.define TEXTCOLOR $64
.define MIRRORTEXTCOLOR $C6

CODE_SEGMENT

.macro scrollline vfbstart, height, bgcolor, fgcolor
.local @line
   ldy #(height-1)
@line:
   sta WSYNC
   
   stx COLUPF
   ldx bgcolor
   stx COLUBK
   stx COLUP0
   stx COLUP1

   ldx vfbstart+$00
   stx PF0
   ldx vfbstart+$08
   stx PF1
   ldx vfbstart+$10
   stx PF2

   ldx vfbstart+$18
   stx PF0
   ldx vfbstart+$20
   stx PF1
   ldx vfbstart+$28
   stx PF2

   eor #$ff
   sta GRP0
   sta GRP1

   ldx fgcolor

   dey
   bpl @line
.endmacro

global_scroller:
   sta WSYNC
	lda INTIM
	cmp #$06
	bne global_scroller
   
scrollerstart:
   ldx schedule
   lda fgcolors,x
   sta fgcolor
   lda mfgcolors,x
   sta mfgcolor
   lda mbgcolors,x
   sta mbgcolor
   lda #$00
   sta VDELP0
   sta VDELP1
   sta COLUP0
   sta COLUP1
   sta NUSIZ0
   sta NUSIZ1
   sta GRP0
   sta GRP1
   sta CTRLPF
   sta WSYNC
   lda #$30
   sta HMP0
   lda #$10
   sta HMP1
   sta RESP0
   ldx #$0b
@loop:
   dex
   bne @loop
   sta RESP1
   sta WSYNC
   sta HMOVE
   
   lda #$00
   sta COLUBK
   sta ENAM0
   sta ENAM1
   sta ENABL

   lda #$55
   sta GRP0
   ldx fgcolor

   scrollline pffb+7, NORMALHEIGHT,   #$00, fgcolor
   scrollline pffb+6, NORMALHEIGHT,   #$00, fgcolor
   scrollline pffb+5, NORMALHEIGHT,   #$00, fgcolor
   scrollline pffb+4, NORMALHEIGHT,   #$00, fgcolor
   scrollline pffb+3, NORMALHEIGHT,   #$00, fgcolor
   scrollline pffb+2, NORMALHEIGHT/2, #$00, fgcolor
   scrollline pffb+2, NORMALHEIGHT/2, mbgcolor, fgcolor
   scrollline pffb+1, NORMALHEIGHT,   mbgcolor, fgcolor
   scrollline pffb+0, NORMALHEIGHT,   mbgcolor, fgcolor
   scrollline pffb+0, MIRROREDHEIGHT, mbgcolor, mfgcolor
   scrollline pffb+1, MIRROREDHEIGHT, mbgcolor, mfgcolor
   scrollline pffb+2, MIRROREDHEIGHT, mbgcolor, mfgcolor
   scrollline pffb+3, MIRROREDHEIGHT, mbgcolor, mfgcolor
   scrollline pffb+4, MIRROREDHEIGHT, mbgcolor, mfgcolor
   scrollline pffb+5, MIRROREDHEIGHT, mbgcolor, mfgcolor
   scrollline pffb+6, MIRROREDHEIGHT, mbgcolor, mfgcolor
   scrollline pffb+7, MIRROREDHEIGHT, mbgcolor, mfgcolor

   sta WSYNC
   lda #$00
   sta PF0
   sta PF1
   sta PF2
   sta COLUPF
   sta GRP0
   sta GRP1
   sta WSYNC
   sta WSYNC
   sta WSYNC
   sta WSYNC
   ;sta COLUBK
   
   bankjmp cont_scroll

