
.include "globals.inc"
.include "locals.inc"

.include "vcs.inc"

.segment "ZEROPAGE"

RODATA_SEGMENT

demopartslo:
   .lobytes partsaddrlist
demopartshi:
   .hibytes partsaddrlist

CODE_SEGMENT

reset:
   cld
   ldx #$ff
   txs
   inx
   txa
   pha
   beq firstrun
   
mainloop:
   php
   pla
   and #%00000100 ; irq flag
   bne nonext
   inc schedule
   ldx #localramstart+1
firstrun:
   sei
@clrloop:
   sta $ff,x
   inx
   bne @clrloop
   
nonext:
   lda schedule
   beq @waitloop
   jsr psmkPlayer
@waitloop:
   bit TIMINT
   bpl @waitloop

   lda #%00001110  ; each '1' bits generate a VSYNC ON line (bits 1..3)
@syncloop:
   sta WSYNC
   sta VSYNC       ; 1st '0' bit resets VSYNC, 2nd '0' bit exit loop
   lsr
   bne @syncloop   ; branch until VSYNC has been reset
   sta VBLANK
   sta COLUBK
   pla
   sta TIM64TI

   ldx schedule
   lda demopartshi,x
   ldy demopartslo,x
   jmp bankjmpcode0
